function [ T p ] = TP_std( z )
%  TP_STD this function computes U.S. STD ATM
%	Computes U.S. Standard Atmosphere profile up to 10.769 km
%	
%  requires: z :: height [m], ARRAY
%	
%  returns:  T :: absolute temp [k]
%            p :: pressure [Pa]

      gamma = 6.5e-3;
      rgas = 287.;
      g = 9.81;
      
      TICE = 273.15;
      T0 = 15. + TICE;
      p0 = 1013.25e2;
      
      T = T0 - gamma .* z;
      p = p0 * (T/T0) .^(g/(rgas*gamma));
      
      return
     
end

