function [y] = desdT_Pa(T)
    % in:         T = temperature in Kelvins
    % out: desdT_Pa = saturation water vapour pressure over water in Pa
    %
    % requires function esat_Pa(T)
    
  L = 2.5e6;    % J/kg
  Rv = 461.5;   % J/(kg K)
                                                                               
  y = (L / Rv) .* esat_Pa(T) ./ T.^2;
  
