%
%-----------------------------------------------------------------------
%
%	Two-Dimension Parcel Model
%
%  This program has been moved to GitLab repo for code management.
%  See commits for more revision information.
%
%  Adam C. Abernathy, adam.abernathy@gmail.com
%  
%  Repo URL:
%  git@gitlab.com:adam_abernathy/atmos-5200.git
%
%  Indended purpose: Homework 1
%
%-----------------------------------------------------------------------
%

%  Scrub
   clear all;
   close all;
   clc;
      
	fprintf('\nTwo-Dimension Cloud Parcel Model\n')

%-----------------------------------------------------------------------
%  Initilization Parameters
%-----------------------------------------------------------------------

   font_base_size = 10;
   descend_parcel = 0;    % yes/no

%  Which plots?
   do_plot_1 = 1;          % Theta and T vs. P
   do_plot_2 = 1;          % Q vs. P

%  Convert cloud water, this is only for HW1-C
   convert_qc = 0;

%  Initial parcel properties
   pMB    = 1000.0;        % pressure (mb)
   TC     = 20;            % temperature (deg C)
   qv     = 14.8e-3;       % water vapor mixing ratio 
   qc     = 0.0;           % liquid water mixing ratio
   qw     = 14.8e-3;

%  Ascent parameters
   dpMB   = 10.0;          % pressure interval (mb)
   ptopMB = 250.0;         % ending pressure (mb)

%
%  Crossing the Rubicon... We will convert all units to SI, prepare
%  variable arrays and then move on to computing the parcel at
%  various levels of pressure.
%

%	Convert units to SI
   t_ice     = 273.15;  % constant
   pa_per_mb = 100.0;   % constant
   kC        = 2*10^-2; % constant, thermodynamic

   p         = pMB * pa_per_mb;
   T         = TC + t_ice;
   dp        = dpMB  * pa_per_mb;
   ptop      = ptopMB * pa_per_mb;

%  Input for adjust
   th = theta(T,p);

%  Set up column arrays for storing results
   n_cycles = ( (pMB - ptopMB) / dpMB );

   if ( descend_parcel == 0 )      % determine array size
      r = 1;
   else
      r = 2;
   end

   p_mb   = zeros(r*n_cycles,1);
   th_K   = zeros(r*n_cycles,1);
   T_K    = zeros(r*n_cycles,1);
   qv_gkg = zeros(r*n_cycles,1);
   qc_gkg = zeros(r*n_cycles,1);
   qw_gkg = zeros(r*n_cycles,1);

%  store initial values (assuming no adjustment req'd)
   i = 1;
   
   p_mb(i)   = pMB;
   th_K(i)   = th;
   T_K(i)    = T;
   qv_gkg(i) = qv * 1.e3;
   qc_gkg(i) = qc * 1.e3;
   qw_gkg(i) = qw * 1.e3;
      
      
%
%  Text output table
%

%  Print unit table
   fprintf('\n\nParameters & Units\n\n')
   fprintf('Var\tDescription\t\tUnit\n')
   fprintf('---------------------------------------------\n')
   fprintf('P\tPressure\t\t[mb]\n')
   fprintf('TH\tTheta\t\t\t[K]\n')
   fprintf('T_{C}\tTemperature\t\t[C]\n')
   fprintf('Q_{v}\tWater Vap Mix Rat.\t[g*kg^-1]\n')
   fprintf('Q_{c}\tLiq Water Mix Rat.\t[g*kg^-1]\n')
   fprintf('---------------------------------------------\n')

   fprintf('\nInitial Conditions...\n')
   fprintf('T_{C}: %3.2f\n',TC)
   fprintf('q_{v}: %6.4f\n',qv)
   fprintf('q_{c}: %6.4f\n',qc)
   fprintf('q_{w}: %6.4f\n',qw)


   fprintf('\nComputations...\n\n')

   fprintf('P\tTH\tT\tqv\tqc\tqw\n')
   fprintf('---------------------------------------------\n')

   fprintf('%4.0f\t%3.2f\t%3.2f\t%2.3f\t%2.3f\t%2.3f\t%2.3f\n',...
            p_mb(i),th_K(i),T_K(i),qv_gkg(i),qc_gkg(i),qw_gkg(i))

%-----------------------------------------------------------------------
%  Ascent & Descend the parcel. We will move it up and down through
%  the atmosphere as a function of pressure.
%-----------------------------------------------------------------------  

   descend_flag = 0;       % is the parcel moving up or down?

   for i = 2 : (2 * n_cycles)

%  Check to see if you need to decend... This assumes that
%  we are bringing the parcel back to the starting point
%  in even incriments.

   if ( i <= n_cycles + 1 )
      p = p - dp;
   elseif ( i >= n_cycles && descend_parcel == 0 )
      continue % get out of the loop
   else

      if ( descend_flag == 0 )
         fprintf('\n\n---------------------------------------------\n')
         fprintf('                  DESCENDING                 \n')
         fprintf('---------------------------------------------\n')
      end

      descend_flag = 1;
      p = p + dp;
   end
      
%  For dry adiabatic ascent: TH, QV and QC do not change. 
%  The 'satadjust' performs an isobaric saturation adjustment

%  For HW1-C we need to convert all the water to have it rain out.
   if ( convert_qc == 1 && descend_flag == 0 )
      temp_qc = 14.8e-3 / 2; % this gets the first plot to work, but not p2
      %temp_qc = 0;
   else
      temp_qc = qc_gkg(i-1) / 1.e3;
   end
      
   [th qv qc qvs pi] = satadjust(th_K(i-1),...
                                 qv_gkg(i-1) / 1.e3,...
                                 temp_qc,...
                                 p);
   

   p_mb(i) = p / pa_per_mb;
   th_K(i) = th;

   T_K(i) = th * pi;

   qv_gkg(i) = qv * 1.e3;
   qc_gkg(i) = qc * 1.e3;
   qw_gkg(i) = (qc + qv) * 1.e3;   

%  Show results and break the lines up a bit for the user
   r=5;
   if ( mod(i,r) == 0 )
      fprintf('\n%4.0f\t%3.2f\t%3.2f\t%2.3f\t%2.3f\t%2.3f\t%2.3f \n',...
            p_mb(i),th_K(i),T_K(i),qv_gkg(i),qc_gkg(i),qw_gkg(i))
   end 

   if ( mod(i,r*5) == 0 )
      fprintf('\n')
   end
           
   end %  End accent loop
      
      
%  Indicate the data is over.
	fprintf('\n---------------------------------------------\n')
 

%-----------------------------------------------------------------------
%  Plot results
%-----------------------------------------------------------------------

%
%  Plot Theta & T vs. P
%
   if ( do_plot_1 == 1 )         
                 
      if ( convert_qc == 1 )
         f  = '../../plots/hw1-part-c-plot-1.png';
         ts = 'Ascent & Decent After Sat-Adjust (Part C)';
      else
         f  = '../../plots/hw1-part-b-plot-1.png';
         ts = 'Ascent After Sat-Adjust (Part B)';
      end

      fig1 = figure(1);
      box on;
      hold on;
      plot(T_K,p_mb,'-b')
      plot(th_K,p_mb,'-r')

      title(ts,'FontSize',font_base_size+2)
      xlabel('Temperature [K]','FontSize',font_base_size)
      ylabel('Pressure [mb]','FontSize',font_base_size )

      set(gca,'YDir','reverse');
      legend('Temp','\Theta','Location','Southwest')
      ylim([floor(log10(ptopMB))*10^(floor(log10(ptopMB))),pMB]);

      set(gcf,'PaperPositionMode','auto')
      set(fig1, 'Position', [0 0 450 350])

      hold off;

      f = sprintf(f);
      print(fig1,'-dpng',f)

   end % do_plot_1


%
%  Plot Q vs. P
%
   if ( do_plot_2 == 1 )

      if ( convert_qc == 1 )
         f  = '../../plots/hw1-part-c-plot-2.png';
         ts = 'Mixing Ratios for Sat-Adjust Ascent & Descent (Part C)';
      else
         f  = '../../plots/hw1-part-b-plot-2.png';
         ts = 'Mixing Ratios for Sat-Adjust Ascent (Part B)';
      end

      fig2 = figure(2);
      box on
      hold on

      plot(qv_gkg,p_mb,'-k')
      plot(qc_gkg,p_mb,'--b')
      plot(qw_gkg,p_mb,'--r')

      title(ts,'FontSize',font_base_size+2)
      xlabel('Saturation [g\cdotkg^{-1}]','FontSize',font_base_size)
      ylabel('Pressure [mb]','FontSize',font_base_size)


      set(gca,'YDir','reverse');
      legend('q_{v}','q_{c}','q_{w}','Location','Southwest')
      ylim([floor(log10(ptopMB))*10^(floor(log10(ptopMB))),pMB]);
      set(gcf,'PaperPositionMode','auto')
      set(fig2, 'Position', [0 0 450 350])

      hold off;
      f = sprintf(f);
      print(fig2,'-dpng',f)

   end % do_plot_2  
        

%  All done!
   fprintf('\n\nComplete.\n\n')
