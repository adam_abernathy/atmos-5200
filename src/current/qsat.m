function [y]=qsat(T,p)
    % in:         T = temperature in Kelvin p = pressure in Pa
    % out:     qsat = saturation water vapor mixing ratio in (kg/kg)

    y = 0.622 .* esat_Pa(T) ./ (p - esat_Pa(T));
