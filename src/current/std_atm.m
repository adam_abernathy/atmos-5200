%***********************************************************************
%     Standard Atmosphere (ATMOS 5200)
%     
%     11-19-14 -- Initial version.
%     11-29-14 -- Updated file saving method.
%
%
%     Dependencies:
%        TP_std.m
%     
%-----------------------------------------------------------------------

%  Scrub
   clear all;
   close all;
   %clc;
      
%  Welcome message
   fprintf('\nPlot U.S. Standard Atmosphere\n')

%-----------------------------------------------------------------------
%  Initilization parameters
%-----------------------------------------------------------------------

   do_plot_1 = 1;
   do_plot_2 = 1;
      
%  Initial parcel properties
   qv = 14.8e-3;        % water vapor mixing ratio 
   qc = 0.;             % liquid water mixing ratio
   qw = 0.5;
   qvs = 0.5;
   rh_i = 0.5;


%  Plot bounds in [kM]
   elev_SFC = 0;
   elev_TOP = 10;

%  Plot resolution in [kM]
   plot_res = .1; % 100 m
      
%-----------------------------------------------------------------------
%  Internal initilization
%-----------------------------------------------------------------------
      
%  Get everything into SI units.
   t_ice     = 273.15;
   pa_per_mb = 100.;
      
%  Define height array
   z_km = elev_SFC:plot_res:elev_TOP;
   z_km = z_km * 1000.;    % Convert from [kM] to [m]

% Storage arrays

   n_profiles = length(z_km);

   qv_gkg   = zeros(n_profiles,1);
   qc_gkg   = zeros(n_profiles,1);
   qw_gkg   = zeros(n_profiles,1);

   rh       = zeros(n_profiles,1);
   qvs_gkg  = zeros(n_profiles,1);

   theta_K  = zeros(n_profiles,1);

%  store initial values (assuming no adjustment req'd)
   i = 1;
   qv_gkg(i) = qv * 1.e3;
   qc_gkg(i) = qc * 1.e3;
   qw_gkg(i) = qw;   % ratio so no units
      
%-----------------------------------------------------------------------

%
%  Computations
%

   [T1 p1] = TP_std(z_km);
   
   T1 = transpose(T1);
   p1 = transpose(p1);
   

   for i = 2 : n_profiles

      th          = theta(T1(i),p1(i));
      theta_K(i)  = th;

      [th qv qc qvs pi] = satadjust(th,qv,qc,p1(i));

      
      qv_gkg(i) = qv;
      qc_gkg(i) = qc;
      qw_gkg(i) = (qv / qc);  % mixing ratio

   end % End [i]

%  Correct the first value in the 'qw' data.
   qw_gkg(1) = qw_gkg(2);

%
%  Plot qw,p vs. T using duel axis method.
%

   if (do_plot_1 == 1)
      f = '../../plots/us_std_atm_mix_press.png';
      fig1 = figure(1);

      %  format xlabel string
      XX_label = sprintf('Temperature [%cC] ', char(176));

      XX = T1 - t_ice;
      
      Y2 = qw_gkg;
      Y1 = p1 ./ pa_per_mb;
      %Y2 = z_km;

      [AX,H1,H2] = plotyy(XX,Y1,XX,Y2);
      grid on;

      set(AX,'YColor','k');
      set(H1,'Linestyle', '--')
      set(H2,'Linestyle', '-')
      set(H1, 'Color', 'k')
      set(H2, 'Color', 'k')

      title('U.S. Standard Atmosphere','FontSize',14)
      xlabel(XX_label)
      
      ylabel(AX(2),'Water Vapor Mixing Ratio [%] ','FontSize',12)
      ylabel(AX(1),'Pressure [mb]','FontSize',12)
      
      legend('Pressure','Mixing Rat.','Location','Southwest')

      

      set(AX(1),'ylim',[200 1000]);
      set(AX(2),'ylim',[0 10]);

      set(AX(1),'YDir','reverse')
      
      axis(AX,'tight')
      
      print(fig1,'-dpng',f)

   end


%
%  Plot z,p vs. T using duel axis method.
%

   if (do_plot_2 == 1)
      f = '../../plots/us_std_atm_press_height.png';
      fig2 = figure(2);

      %  format xlabel string
      XX_label = sprintf('Temperature [%cC] ', char(176));

      XX = T1 - t_ice;
      
      %Y2 = qw_gkg;
      Y1 = p1 ./ pa_per_mb;
      Y2 = z_km;

      [AX,H1,H2] = plotyy(XX,Y1,XX,Y2);
      grid on;

      set(AX,'YColor','k');
      set(H1,'Linestyle', '--')
      set(H2,'Linestyle', '-')
      set(H1, 'Color', 'k')
      set(H2, 'Color', 'k')

      title('U.S. Standard Atmosphere','FontSize',14)
      xlabel(XX_label)
      
      ylabel(AX(2),'Height [m] ','FontSize',12)
      ylabel(AX(1),'Pressure [mb]','FontSize',12)
      
      legend('Pressure','Height','Location','Southwest')

      

      set(AX(1),'ylim',[200 1000]);
      set(AX(2),'ylim',[0 10]);

      set(AX(1),'YDir','reverse')
      
      axis(AX,'tight')
      
      print(fig2,'-dpng',f)

   end


%  All done!
	fprintf('\n\nComplete.\n\n');
