%***********************************************************************
%     Parcel Model for Cloud Physics (ATMOS 5200)
%     
%     11-06-14 -- Adapted code from S. Krugers original model.
%     11-18-14 -- Completed HW1, criteria. Model appears to work.
%     11-19-14 -- Modified to plot on skew-t plot.
%     11-24-14 -- THIS VERSION IS MODIFIED FOR HW-2B!
%     11-25-14 -- Updated array sizing method
%
%
%     Dependencies:
%        esat_Pa.m
%        desdT_Pa.m
%        satadjust.m
%        theta.m
%        tskew.m
%        st_atm.m
%
%     BE SURE TO HAVE AN 'ouput' FOLDER LOCATED IN THE WORKING DIR!
%     
%***********************************************************************


%**************************************************************************
%     Kickoff & Scrub
%**************************************************************************
      close all;
      clear all;
      clc;

%  Conversions & constants
      TICE = 273.15;
      pa_per_mb = 100.;
      
      gamma = 6.5e-3;
      rgas = 287.;
      g = 9.81;
      
      
%**************************************************************************
%     Initilization Parameters
%**************************************************************************

%  Create plot at the end? [0] No, [1] Yes
      do_plot_1 = 1;
      skew_TOA = 250;
      skew_SFC = 1050;
      skew_tmin = -20;
      skew_tmax = 20;
      
%  Hold Theta [K] constant? [0] No, [1] Yes
      hold_theta = 0;
      
%  Initial height of parcel (m)
      z = 0; 
      dz = 100;
      ztop = 1000;

%  Relative humidity in environment at all heights      
      RHe1 = .5;		
      
%  T (K) and p (Pa) in environment at initial height, use Std ATM.
      [Te pe] = TP_std(z);	
      
      
%  Parcel properties at initial height
      thC = 16;         % potential temp (C)
      qv = 14.8e-3;		% mixing ratio (kg/kg) 
      qc = 0;           % liquid water mixing ratio (kg/kg)
      p = pe;           % pressure (Pa)
      th = theta(Te,p);	% potential temperature (K)
      T = Te;           % temperature (K)
      lambda = 0.1;


      
%**************************************************************************
%     Internal inilization
%**************************************************************************
      
%  Set up column arrays for storing results
      % Added dynamic array sizing. I know this is not F77 friendly, but
      % it will work in F95.
      n_elements = (ztop/dz)+1;
      
      p_mb = zeros(n_elements,1)*NaN ;
      th_K = zeros(n_elements,1)*NaN;
      T_C = zeros(n_elements,1)*NaN;
      
      qv_gkg = zeros(n_elements,1)*NaN;
      qc_gkg = zeros(n_elements,1)*NaN;        
      qvs_gkg = zeros(n_elements,1)*NaN; 
      
      Te_C = zeros(n_elements,1)*NaN;
      RHe = zeros(n_elements,1)*NaN;
      
      % Just in case we want element by element... 
      %CAPE = zeros(n_elements,1)*NaN;
      %LFC = zeros(n_elements,1)*NaN;
      %LNB = zeros(n_elements,1)*NaN;
      
      print_line = strcat('--------------------------------------',...
                          '-------------------------');

% Store initial values (assuming no adjustment req'd)
      i = 1;
      p_mb(i) = p/pa_per_mb;
      th_K(i) = th;
      T_C(i) = T - TICE;
      
      qv_gkg(i) = qv * 1.e3;
      qc_gkg(i) = qc * 1.e3;
      qvs_gkg(i) = 0.;        % parcel saturation mixing ratio (g/kg)      
      
      Te_C(i) = thC;          % temperature in environment (C)
      RHe(i) = RHe1;          % relative humidity in environment

% Print initial values

      if (hold_theta == 1)
         ic = 1;
      else
         ic = i;
      end
      
      fprintf('Homework 2B\n\n')
      fprintf('\nCOMPUTATIONS...\n\n')
      
      fprintf('P\tTH\tT\tqv\tqc\tCAPE\tLNB\tLFC\n')
      
      s = strcat(print_line,'\n');
      fprintf(s)

      fprintf('%4.0f\t%3.2f\t%3.2f\t%2.3f\t%2.3f\t%2.3f\n',...
               p_mb(i),th_K(ic),T_C(i),qv_gkg(i),qc_gkg(i))

            
%**************************************************************************
%     Computations
%**************************************************************************            
      
%  CAPE
      CAPE=0;
      LFC=0;
      LNB=0;


      while (z < ztop)

      i = i + 1;  
      z = z + dz;
      
%  For CAPE: save T, Te, p from previous level
      T1 = T;
      Te1 = Te;
      p1 = p; 

%  Environment T, p (US Standard Atmosphere

      % T (K) and p (Pa) in environment at height z
      pe = p*pa_per_mb;
      [Te pe]=TP_std(z);
      
      p = pe;		% parcel pressure = environment pressure

%  Environment theta, qv, qc		% (NEW)
      the = theta(Te,p);		% (K)
      qve = qv;               % (kg/kg)
      qce = qc;		

%  FOR DRY ADIABATIC ASCENT TH, QV, QC DO NOT CHANGE !

%  Entrainment 
      th = the;
      qv = qve;
      qc = qce;         

%  ADJUST PERFORMS ISOBARIC SATURATION ADJUSTMENT
      th=th-lambda*(th-the)*dz;
      [th qv qc qvs pi] = satadjust(th,qv,qc,p);

      p_mb(i) = p/pa_per_mb;
      th_K(i) = th;
      T_C(i) = th*pi-TICE;
      
      qv_gkg(i) = qv * 1.e3;
      qc_gkg(i) = qc * 1.e3;     
      qvs_gkg(i) = qvs * 1.e3;	% parcel saturation mixing ratio (g/kg) 
      
      Te_C(i) = th*pi-TICE;				% temperature in environment (C)
      RHe(i) = qv_gkg(i) / qvs_gkg(i); % relative humidity in environment
     

%
%  SOMETHING IS FUNNY HERE!
%  Compute CAPE
      dCAPE =  rgas * ( T - Te + T1 - Te1 ) / 2 * log(p1/p);
      CAPE = CAPE + max(0, dCAPE);

%  LFC: dCAPE becomes > 0
      if T1-Te1 < 0 && T-Te > 0 
          LFC = (p1 + p) / 2 / pa_per_mb;
      end
%  LNB: dCAPE becomes < 0
      if T1-Te1 > 0 && T-Te < 0 
          LNB = (p1 + p) / 2 / pa_per_mb;
      end

%
%
      
      
%  Print values       
      if (hold_theta == 1)
         ic = 1;
      else
         ic = i;
      end
   
 fprintf('\n%4.0f\t%3.2f\t%3.2f\t%2.3f\t%2.3f\t%4.2f\t%4.0f\t%4.0f',...
               p_mb(i),th_K(i),T_C(i),qv_gkg(i),qc_gkg(i),CAPE,...
               LNB,LFC)
            
%  Break the lines up a bit for the user
      r=5;
      if (mod(i,r) == 0)
         fprintf('\n')
      end
      
%  End computation loop
      end

%  Indicate the data is over.
      s = strcat('\n',print_line,'\n');
      fprintf(s)
      
      %  Print wMax
      wmax = sqrt(2*CAPE);
      fprintf('\nw max: %4.3f\n\n',wmax)

%     Do Plot?
      if (do_plot_1 == 1)
         fprintf('\nStarting plot...\n');
         
         tskew(p_mb,T_C,qv_gkg./qvs_gkg); % parcel
         hold on;
         tskew(p_mb,Te_C,RHe)	 ;      		% environment
         axis([skew_tmin skew_tmax skew_TOA skew_SFC]);   		% zoom in
         
         orient landscape
         print -dpsc2 'hw2B_plots\\Std_Atm_skewt.ps'

         fprintf('\nPlot complete. Saved.\n');
      else
         fprintf('\nPlot disabled.\n');
      end
      
 %  All done!
      fprintf('\n\nComplete.\n\n');
      